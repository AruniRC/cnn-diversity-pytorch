# run_cifar_lenet_widen.py

import os
import torch
import torchvision
import torch.optim as optim
from torch.optim.lr_scheduler import MultiStepLR
from vis_utils import *
from model_def import *
from model_train import *




def trainNetwork(expName, batchSize, useGpu, modelPath, numEpochs, 
                 learningRate, numNewFilters):
    print expName

    # create experiment folder
    expDir = os.path.join('./data', expName)
    if not os.path.exists(expDir):
        os.makedirs(expDir)

    # setup and load CIFAR dataset
    trainloader, testloader, classes = setup_cifar_data(batchSize)

    # get a LeNet network
    if not modelPath:
        # create from scratch is `modelPath` is empty
        net = NetWide(numNewFilters)
    else:
        # load network from provided `modelPath`
        net = NetWide(numNewFilters)
        net.load_state_dict(torch.load(modelPath))

    print(net)


    # Define a Loss function and optimizer settings
    criterion = nn.CrossEntropyLoss()
    optimizer = optim.SGD(net.parameters(), lr=learningRate, momentum=0.9, weight_decay=5e-4)
    # scheduler = optim.lr_scheduler.MultiStepLR(optimizer, milestones=milestones, gamma=gamma)

    # Train the network
    train_cifar_net(net, trainloader, testloader, criterion, optimizer, expDir, 
        batchSize=batchSize, numEpochs=numEpochs, useGpu=useGpu, fixFilterList=[], verboseFrequency=100)

    # Save the network
    #    to be loaded as:
    #       net = Net()
    #       net.load_state_dict(torch.load(PATH))
    torch.save(net.state_dict(), os.path.join(expDir,'net-trained.dat'))

    if useGpu:
        net.cuda()

    train_accu = accuracy_on_dataset(net, trainloader, useGpu)
    test_accu = accuracy_on_dataset(net, testloader, useGpu)

    # Performance on train set
    print('Accuracy of the network on the 50000 training images: %d %%' % (
            train_accu))

    # Performance on test set
    print('Accuracy of the network on the 10000 test images: %d %%' % (
            test_accu))

    # Per-class accuracy on test set
    print_cifar_per_class_accuracy(net, classes, testloader, useGpu)

    # save results to file
    f_res = open(os.path.join(expDir,'result-train-test.txt', 'w'))
    f_res.write(str(train_accu) + '\n')
    f_res.write(str(test_accu) + '\n')
    f_res.close()





if __name__ == '__main__':

    # specify experiment settings
    #   cifar-lenet-widen   - keep adding new filters and train
    #   cifar-lenet-widen-v1.0      - 20 epochs, lr=0.01
    #   cifar-lenet-widen-v1.1      - 10 epochs, lr=0.001
    
    batchSize = 32
    useGpu = True
    numNewFilters = 10

     # specify path if resuming training from saved model, else []
    
    # step 1
    expName = 'cifar-lenet-widen-v1.0'
    modelPath = []
    numEpochs = 20
    learningRate = 0.01
    trainNetwork(expName, batchSize, useGpu, modelPath, 
        numEpochs, learningRate, numNewFilters)


    # step 2
    expName = 'cifar-lenet-widen-v1.1'
    modelPath = 'data/cifar-lenet-widen-v1.0/net-trained.dat'
    numEpochs = 10
    learningRate = 0.001
    trainNetwork(expName, batchSize, useGpu, modelPath, 
        numEpochs, learningRate, numNewFilters)
