#!/home/arunirc/pytorch-cifar/bin/python

# On Gypsum cluster, run using: ./run_cifar_mlp.py
# On local machine, (inside a conda env), use: python run_cifar_mlp.py

import os
import torch
import torchvision
import torch.optim as optim
from torch.optim.lr_scheduler import StepLR
import matplotlib.pyplot as plt
import numpy as np
import json
import argparse
from vis_utils import *
from model_def import *
from model_train import *


def parse_input_opts():
    parser = argparse.ArgumentParser(description='Train a 2-layer MLP network on CIFAR-10')
    parser.add_argument('expName', help='specify experiment name')
    parser.add_argument('--outDir', default='./data')
    parser.add_argument('--batchSize', default=100, type=int)
    parser.add_argument('--useGpu', default=True, type=bool)
    parser.add_argument('--seed', default=0, type=int)
    parser.add_argument('--modelPath', default=[])
    parser.add_argument('--numEpochs', default=10, type=int)
    parser.add_argument('--learningRate', default=0.03, type=float)
    parser.add_argument('--momentum', default=0.9, type=float)
    parser.add_argument('--weight_decay', default=0.018, type=float)
    parser.add_argument('--gamma', default=0.9, type=float)
    parser.add_argument('--w1', default=100, type=int)
    parser.add_argument('--w2', default=100, type=int)
    parser.add_argument('--numNewFilter', default=0, type=int)
    parser.add_argument('--fixFilterList', default=0, type=int)
    parser.add_argument('--newWeightLrMult', default=1, type=int)
    parser.add_argument('--verboseFrequency', default=50, type=int)
    parser.add_argument('--doVisFilter', default=False, type=bool, \
                        help='save filters every `verboseFrequency` batches - SLOW!')
    parser.add_argument('--doTrainPlot', default=False, type=bool, \
                        help='plot training curves every epoch')
    opts = parser.parse_args()

    return opts


if __name__ == '__main__':

    # --------------------------------------------------------------------------
    # Parse input experiment settings
    # --------------------------------------------------------------------------
    opts = parse_input_opts()
    # expName = 'cifar-MLP_v6.0.1'
    
    # Set experiment config
    useGpu = opts.useGpu
    seed = opts.seed  # for repeatable results
    modelPath = opts.modelPath
    numEpochs = opts.numEpochs
    learningRate = opts.learningRate 
    momentum = opts.momentum
    weight_decay = opts.weight_decay
    gamma = opts.gamma
    w1 = opts.w1
    w2 = opts.w2
    verboseFrequency = opts.verboseFrequency
    doVisFilter = opts.doVisFilter
    doTrainPlot = opts.doTrainPlot

    # --------------------------------------------------------------------------
    # Setup output locations
    # --------------------------------------------------------------------------
    # create experiment folder
    if not os.path.exists(opts.outDir):
        expDir = os.path.join('./data', opts.expName)
    else:
        expDir = os.path.join(opts.outDir, opts.expName)
    if not os.path.exists(expDir):
        os.makedirs(expDir)

    # save training config 
    # (NOTE: changing values in the JSON will not reflect in training)
    cfg = {'useGpu': useGpu, 'numEpochs': numEpochs, \
           'learningRate': learningRate, \
           'batchSize': opts.batchSize, 'momentum': momentum, \
           'weight_decay': weight_decay, \
           'gamma': gamma, 'w1': w1, 'w2': w2, \
           'verboseFrequency': verboseFrequency, 'doVisFilter': doVisFilter, \
           'numNewFilter': opts.numNewFilter, \
           'newWeightLrMult': opts.newWeightLrMult, \
           'fixFilterList': opts.fixFilterList}

    # cfg = json.load(file(os.path.join(expDir,'train_config.json'), 'r'))
    with open(os.path.join(expDir,'train_config.json'), 'w') as config_file :
        json.dump(cfg, config_file, indent=4, separators=(',', ': '), \
                                              sort_keys=True)


    # --------------------------------------------------------------------------
    # Data setup
    # --------------------------------------------------------------------------
    # setup and load CIFAR dataset
    trainloader, testloader, classes = setup_cifar_data_mlp(opts.batchSize)
     

    # --------------------------------------------------------------------------
    # Model setup
    # --------------------------------------------------------------------------
    # create a fully-connected network (MLP) with ReLU non-linearities
    #   - input images are vectorized images of 32x32x3 (3072 dim)
    #   - two hidden layers both with 100 units (filters)
    net = MLP(input_dim=3072, fc1_dim=w1, fc2_dim=w2)
    # net._init_weights(init_sigma=0.003) # init using Gaussians (TODO: expose option)

    # if a path is provided then load network from that location
    if len(opts.modelPath) > 0:
        print 'Loading network from modelPath %s' % opts.modelPath
        net.load_state_dict(torch.load(opts.modelPath))

    # add a new filter (init to 1's) to the first layer
    if opts.numNewFilter > 0:
        net._add_new_weight(opts.numNewFilter, init_sigma=0.003)       


    # set random seed
    torch.default_generator.manual_seed(seed)
    if useGpu:
        torch.cuda.manual_seed_all(seed)

    # Define a Loss function and optimizer settings
    criterion = nn.CrossEntropyLoss()
    optimizer = optim.SGD(net.parameters(), lr=learningRate, momentum=momentum,\
                                            weight_decay=weight_decay)
    numBatchesPerEpoch = len(trainloader.dataset) // opts.batchSize
    lr_scheduler = optim.lr_scheduler.StepLR(optimizer, 1, gamma=gamma)


    # --------------------------------------------------------------------------
    # Train the network    
    # --------------------------------------------------------------------------
    if not os.path.isfile(os.path.join(expDir, 'net-trained.dat')):
        # Train the network
        train_cifar_net(net, trainloader, testloader, criterion, \
                        (optimizer,lr_scheduler), expDir, \
                        batchSize=opts.batchSize,
                        numEpochs=numEpochs, useGpu=useGpu, \
                        fixFilterList=opts.fixFilterList, \
                        newWeightLrMult=opts.newWeightLrMult, \
                        numNewFilter=opts.numNewFilter, \
                        verboseFrequency=verboseFrequency, \
                        doVisFilter=doVisFilter, \
                        doTrainPlot=doTrainPlot)

        # Save the network
        #    to be loaded as:
        #       net = Net()
        #       net.load_state_dict(torch.load(PATH))
        torch.save(net.state_dict(), os.path.join(expDir, 'net-trained.dat'))
    else:
        print 'Loading network state dict from file.'
        net.load_state_dict(torch.load(os.path.join(expDir, 'net-trained.dat')))
        print 'Done.'


    # --------------------------------------------------------------------------
    # Filter similarity
    # --------------------------------------------------------------------------
    fc1Params = list(net.fc1.parameters())
    w = fc1Params[0].data.numpy() # filters
    similMat, w = get_layer_cosine_similarity(net, 'fc1.weight') # returned as numpy arrays
    np.save(os.path.join(expDir,'simil-mat'), similMat)
    np.save(os.path.join(expDir,'w-mat'), w)
    smat = np.load(os.path.join(expDir,'simil-mat.npy'))
    w_mat = np.load(os.path.join(expDir,'w-mat.npy'))