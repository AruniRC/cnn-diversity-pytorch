#!/usr/bin/env bash

# Download multiple output files from remote cluster gypsum.cs.umass.edu
#
# 1. create a secure file with remote's SSH password (~/.ssh/pwd)
# 2. install sshpass
# 3. run this script with changes as necessary
#
# NOTE: This assumes each job on Gypsum saved its outputs in a folder like:
#           <exptName>-0/*.npy, <exptName>-1/*.npy, <exptName>-2/*.npy, ...
#       These outputs are rsynced as <local_folder>/<local_folder>-0, 1, 2, ...

LOCAL_FOLDER='data/cifar-MLP_v6.1/cifar-MLP_v6.1'
REMOTE_LOGIN='arunirc@gypsum.cs.umass.edu'
# REMOTE_PREFIX='/mnt/nfs/work1/elm/arunirc/cifar-MLP_v6.2'
REMOTE_PREFIX='/home/arunirc/cnn-diversity-pytorch/data/cifar-MLP_v6.1'
REMOTE_SUFFIX='*.dat'

for i in {0..99}
do
    mkdir -p ${LOCAL_FOLDER}'-'$i
    sshpass -p $(cat ~/.ssh/pwd) rsync \
    -azP ${REMOTE_LOGIN}:${REMOTE_PREFIX}'-'$i'/'${REMOTE_SUFFIX} \
    ${LOCAL_FOLDER}'-'$i'/'
done

