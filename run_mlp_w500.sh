#!/usr/bin/env bash

# Trains an MLP multiple times by supplying a different seed to
# `run_cifar_mlp.py` Python script.
# The results are saved as <expName>-<ID>, where seed is used as ID.

EXP_NAME='cifar-MLP_v6.2'
OUT_DIR='/mnt/nfs/work1/elm/arunirc/'
NUM_EPOCHS=100
W1=500
LOG_DIR=${EXP_NAME}'-job-log'
mkdir -p ${LOG_DIR}

START=$(date +%s.%N)
for i in {0..99}
do
    echo 'Expt run - '$i
    SEED=$i
    EXP_FOLDER_NAME=${EXP_NAME}'-'${SEED}
    echo ${EXP_FOLDER_NAME}
    OUT_FILE=${LOG_DIR}'/cifar_mlp_'${SEED}'.out'
    ERR_FILE=${LOG_DIR}'/cifar_mlp_'${SEED}'.err'
    # INFO: sbatch was causing problems here - could not import from local Python source files.
    #       srun, strangely, was quite ok with this!
    srun -o ${OUT_FILE} -e ${ERR_FILE} -p titanx-short --gres=gpu:1 run_cifar_mlp.py ${EXP_FOLDER_NAME} --seed ${SEED} --numEpochs ${NUM_EPOCHS} --w1 ${W1} --outDir=${OUT_DIR}&
    echo 'Expt done - '$i
done

# TODO - log elapsed time
END=$(date +%s.%N)
DIFF=$(echo "$END - $START" | bc)
echo 'Time elapsed: '$DIFF