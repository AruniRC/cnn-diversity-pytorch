import os
import torch
import torchvision
import torch.optim as optim
from vis_utils import *
from model_def import *
from model_train import *


# specify experiment settings
#	cifar-lenet-v0		- 10 epochs, lr=0.01
#	cifar-lenet-v0.1	- 5 epochs, lr=0.01  (the loss function oscillates)
#	cifar-lenet-v0.2	- 10 epochs, lr=0.001
#	cifar-lenet-v0.3	- 10 epochs, lr=0.001
#	cifar-lenet-v0.4	- 10 epochs, lr=0.001
#	cifar-lenet-v0.5	- 10 epochs, lr=0.001
#	TODO cifar-lenet-v2	- keep adding new filters and train
expName = 'cifar-lenet-v0.5'
batchSize = 32
useGpu = True
numEpochs = 10
modelPath = 'data/cifar-lenet-v0.4/net-trained.dat' # specify path if resuming training from saved model, else []
learningRate = 0.001

# -----------------------------------------------------------------------------


# create experiment folder
expDir = os.path.join('./data', expName)
if not os.path.exists(expDir):
    os.makedirs(expDir)


# setup and load CIFAR dataset
trainloader, testloader, classes = setup_cifar_data(batchSize)

# create a LeNet network
if not modelPath:
    # create from scratch
    net = Net()
else:
    # load network from provided `modelPath`
    net = Net()
    net.load_state_dict(torch.load(modelPath))

print(net)


# Define a Loss function and optimizer settings
criterion = nn.CrossEntropyLoss()
optimizer = optim.SGD(net.parameters(), lr=learningRate, momentum=0.9, weight_decay=5e-4)

# Train the network
train_cifar_net(net, trainloader, testloader, criterion, optimizer, expDir, batchSize=batchSize,
                numEpochs=numEpochs, useGpu=useGpu, fixFilterList=[], verboseFrequency=100)


# Save the network
#    to be loaded as:
#       net = Net()
#       net.load_state_dict(torch.load(PATH))
torch.save(net.state_dict(), os.path.join(expDir,'net-trained.dat'))


if useGpu:
    net.cuda()

# Performance on train set
print('Accuracy of the network on the 50000 training images: %d %%' % (
    accuracy_on_dataset(net, trainloader, useGpu)))

# Performance on test set
print('Accuracy of the network on the 10000 test images: %d %%' % (
    accuracy_on_dataset(net, testloader, useGpu)))

# Per-class accuracy on test set
print_cifar_per_class_accuracy(net, classes, testloader, useGpu)


