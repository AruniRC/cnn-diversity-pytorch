# run_cifar_lenet_freeze_filter.py

import os
import torch
import torchvision
import torch.optim as optim
from torch.optim.lr_scheduler import MultiStepLR
from vis_utils import *
from model_def import *
from model_train import *




def trainNetwork(expName, batchSize, useGpu, modelPath, numEpochs, 
                 learningRate, numNewFilters, doFreeze):
    print('Experiment: ' + expName)

    # create experiment folder
    expDir = os.path.join('./data', expName)
    if not os.path.exists(expDir):
        os.makedirs(expDir)

    # setup and load CIFAR dataset
    trainloader, testloader, classes = setup_cifar_data(batchSize)

    if not modelPath:
        # create network from scratch if `modelPath` is empty
        net = NetWide(numNewFilters)
    else:
        # load saved network if provided in `modelPath`
        net = NetWide(numNewFilters)
        net.load_state_dict(torch.load(modelPath))

    print(net)

    # decide which filters to freeze in conv1
    conv1Params = list(net.conv1.parameters())
    (numFilters,_,_,_) = conv1Params[0].size()
    fixFilterList = range(numFilters-numNewFilters, numFilters)


    # Define a Loss function and optimizer settings
    criterion = nn.CrossEntropyLoss()
    optimizer = optim.SGD(net.parameters(), lr=learningRate, momentum=0.9, weight_decay=5e-4)
    # scheduler = optim.lr_scheduler.MultiStepLR(optimizer, milestones=milestones, gamma=gamma)

    if not os.path.isfile(os.path.join(expDir,'net-trained.dat')):
        # Train the network
        train_cifar_net(net, trainloader, testloader, criterion, optimizer, expDir, 
            fixFilterList=fixFilterList,
            batchSize=batchSize, numEpochs=numEpochs, 
            useGpu=useGpu, verboseFrequency=100)

        # Save the network
        #    to be loaded as:
        #       net = Net()
        #       net.load_state_dict(torch.load(PATH))
        torch.save(net.state_dict(), os.path.join(expDir,'net-trained.dat'))
    else:
        # load saved network if already trained before
        net = NetWide(numNewFilters)
        net.load_state_dict(torch.load(os.path.join(expDir,'net-trained.dat')))


    if useGpu:
        net.cuda()


    # Display (and save to file) model performance
    train_accu = accuracy_on_dataset(net, trainloader, useGpu)
    test_accu = accuracy_on_dataset(net, testloader, useGpu)

    # performance on train set
    print('Accuracy of the network on the 50000 training images: %d %%' % (
            train_accu))

    # performance on test set
    print('Accuracy of the network on the 10000 test images: %d %%' % (
            test_accu))

    # per-class accuracy on test set
    print_cifar_per_class_accuracy(net, classes, testloader, useGpu)

    # save results to file
    f_res = open(os.path.join(expDir,'result-train-test.txt'), 'w')
    f_res.write(str(train_accu) + '\n')
    f_res.write(str(test_accu) + '\n')
    f_res.close()



if __name__ == '__main__':
    # Start from a 16-filter conv1 trained network, 
    #   add new filters to conv1,
    #   fix previous filters,
    #   train till convergence.
    #
    # TODO - specify experiment settings
    #   cifar-lenet-widen   - keep adding new filters and train
    #   cifar-lenet-widen-v1.0      - 20 epochs, lr=0.01
    #   cifar-lenet-widen-v1.1      - 10 epochs, lr=0.001
    
    batchSize = 32
    useGpu = True
    seed = 0  # for repeatable results
    numFilterList = range(10,30,10) # list of how many filters to be added to conv1
    expName = 'cifar-freeze-testing'
    doFreeze = True

    for idx, numNewFilters in enumerate(numFilterList):

        # set random seed
        torch.default_generator.manual_seed(seed)
        torch.cuda.manual_seed_all(seed)
        # NOTE: this does not affect non-deterministic behaviour of CuDNN

        # specify path if resuming training from saved model, else []
        
        # step 1
        expName_1 = expName+'-'+str(numNewFilters)+'-00'
        if idx == 0:
            # start from a 16-filter conv1 pre-trained network
            modelPath = os.path.join('./data', 'cifar-lenet-widen-10-01', 'net-trained.dat') 
            print modelPath
            doFreeze = False
        else:
            # start from network trained at previous setting within this loop
            prevExpName = expName + '-' + str(numFilterList[idx-1]) + '-01'
            modelPath = os.path.join('./data', prevExpName, 'net-trained.dat')
            print modelPath
            doFreeze = True

        numEpochs = 20
        learningRate = 0.01
        trainNetwork(expName_1, batchSize, useGpu, modelPath, 
            numEpochs, learningRate, numNewFilters, doFreeze)


        # step 2
        expName_2 = expName+'-'+str(numNewFilters)+'-01'
        # use saved model from previous stage as starting point
        modelPath = os.path.join('./data', expName_1, 'net-trained.dat') 
        numEpochs = 10
        learningRate = 0.001
        trainNetwork(expName_2, batchSize, useGpu, modelPath, 
            numEpochs, learningRate, numNewFilters)
