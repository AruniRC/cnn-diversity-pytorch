Diversity of CNN features
=========================

## Setup with the code

Most of the methods are implemented in the files `vis_utils.py`, `model_def.py` and `model_train.py`.
The other files are "run scripts" that call these methods under various settings. The simplest way to get started would be to browse through the iPython Notebooks mentioned below.

**Dependencies**: PyTorch, PIL, SciPy, Numpy, sklearn, tqdm, matplotlib, jupyter, iPython.


### Fully-connected network (MLP) 

##### Basics of training
Use the `run_cifar_train_mlp_replicate` iPython Notebook to first train a fully-connected network 
(or MLP) on CIFAR-10. This mostly follows standard hyper-parameter settings -- importantly, we set 
a pretty high L2-regularization (equivalent to "weight decay" in neural net parlance) on the weights.
This gives rise to smooth filters that can be visualized inside that Notebook, along with accuracy 
plots and other diagnostics.

**NOTE:** the above step runs for 50 epochs, but will 
still take a long time to finish unless you have a GPU. In fact, the `useGpu` flag is set to TRUE, so if you don't have a GPU available this will trigger an
error by default.


##### Network reduction
The `net_reduce_cifar_mlp` Notebook trains an MLP network and runs through the steps of pruning duplicate filters and comparing to a simple baseline of removing filters based on their L1-norm.

##### Dropout
`net_dropout_cifar_mlp` Notebook can be used to change the level of dropout and log the results.

##### Distribution of cosine similarity
`backup_notebooks/plot_cosine_distribution.ipynb`: plots the distribution of cosine similarities between the filters (or neurons) of a two-layer fully-connected network (MLP). This illustrates the variation of the *first* kind of redundancy (*duplicate filter*) in neural networks with layer size.

##### Distribution of filter norm
`backup_notebooks/plot_norm_distribution.ipynb`: This Notebook plots the distrbution of L2-norms for the first-layer filters in MLP networks. This would illustrate how the *second* kind of redundancy (having *extremely small weights*) varies with layer size.

##### Filter group norms
Do all MLP filters in the same group have the same norm? Plot norms of filter groups: `plot_duplicate_norms.ipynb`


### Modified LeNet (CNN) 

##### Network reduction
Training and reducing a CNN on CIFAR-10: `net_reduce_cifar_cnn`. Includes step-by-step walkthrough of the process.


### Similarity with layer size
After networks have been trained and saved, `plot_filter_similarity` generates curves showing how *filter similarity* varies with *layer size* for both CNNs and MLPs. The last section plots the *evolution of filter similarity over training iterations*.








